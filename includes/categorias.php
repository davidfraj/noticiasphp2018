

<?php  
////////////////////////////////////////////////
/////////LISTADO DE CATEGORIAS
////////////////////////////////////////////////
?>
<h3>Categorias</h3>

<ul class="nav nav-pills nav-stacked">

<?php  
//Pongo active el enlace a TODAS, si no le paso idCategoria
if(isset($_GET['idCategoria'])){
	$activo='';
	$idCategoria=$_GET['idCategoria'];
}else{
	$activo='active';
	$idCategoria=0;
}
?>
<li class="<?php echo $activo; ?>">
	<a href="index.php?p=noticias.php">
		Todas
	</a>
</li>
<?php  
//Establezco la consulta
$sql="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
//Ejecuto la consulta
$consulta=$conexion->query($sql);
//Proceso los resultados
while($registro=$consulta->fetch_array()){
	if($idCategoria==$registro['idCategoria']){
		$activo='active';
	}else{
		$activo='';
	}
	?>
	<li class="<?php echo $activo; ?>">
		<a href="index.php?p=noticias.php&idCategoria=<?php echo $registro['idCategoria']; ?>">
			<?php echo $registro['nombreCategoria'];?>
		</a>
	</li>
	<?php
}
?>
</ul>
<hr>
