


<h3>
	Modificar una categoria
	-
	<small>
		<a href="index.php?p=categorias.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if(isset($_POST['enviar'])){
	//modifico la categoria
	//Recojo los datos que quiero modificar
	$nombre=$_POST['nombre'];
	$id=$_POST['idCategoria'];

	//Establezco consulta
	$sql="UPDATE categorias SET nombreCategoria='$nombre' WHERE idCategoria=$id";

	//ejecuto la consulta
	if($consulta=$conexion->query($sql)){
		//header('location:index.php?p=categorias.php');
		header('Refresh: 2; url=index.php?p=categorias.php');
		?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Realizado con exito
			<img src="images/cargando.gif" width="50">
		</div>
		<?php
	}else{
		?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}

}else{
	//Muestro el formulario de modificacion
	//Necesito el id de la categoria que quiero modificar
	//para rellenar el formulario con los datos de dicha categoria
	$idCategoria=$_GET['idCategoria'];
	$sql="SELECT * FROM categorias WHERE idCategoria=$idCategoria";
	$consulta=$conexion->query($sql);
	$registro=$consulta->fetch_array();
?>

<form action="index.php?p=modificarCat.php" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="nombre">Nombre de la categoria:</label>
		<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $registro['nombreCategoria']; ?>">
	</div>

	<input type="hidden" name="idCategoria" value="<?php echo $idCategoria; ?>">

	<button type="submit" name="enviar" class="btn btn-default">
		Enviar
	</button>

</form> 

<?php } ?>