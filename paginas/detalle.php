


<h3>
	Detalle de noticia - 
	<small>
		<a href="index.php?p=noticias.php">Volver</a>
	</small>
</h3>

<?php  
//Recojo el id de Noticia que quiero mostrar
$idNoticia=$_GET['idNoticia'];

//Establezco una consulta segun ese id de Noticia
$sql="SELECT * FROM noticias INNER JOIN categorias ON categorias.idCategoria=noticias.idCategoria WHERE idNoticia=$idNoticia";

//Ejecuto la consulta
$consulta=$conexion->query($sql);

//Extraigo los datos de dicha consulta (Como solo tengo 1)
$registro=$consulta->fetch_array();
?>

<article>
	<header>
		<h4>
			<strong>
			<?php echo $registro['tituloNoticia']; ?>
			</strong>
		</h4>
		<small>
			<?php echo $registro['nombreCategoria'];?>
		</small>
	</header>
	<section>

		<img src="images/<?php echo $registro['imagenNoticia']; ?>" class="img-responsive img-rounded">

		<br>
		
		<?php echo $registro['textoNoticia']; ?>

	</section>
	<footer class="text-right" style="clear: both;">
		<?php echo $registro['fechaNoticia']; ?>
	</footer>
</article>