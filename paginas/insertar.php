


<h3>
	Insertar una noticia
	-
	<small>
		<a href="index.php?p=noticias.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if(isset($_POST['enviar'])){
	//Inserto la noticia
	//Recojo los datos que quiero insertar
	$titulo=$_POST['titulo'];
	$texto=$_POST['texto'];
	$nombreImagen=$_FILES['imagen']['name']; //Nombre del archivo imagen

	$categoria=$_POST['categoria'];

	//MUEVO la imagen a la carpeta
	move_uploaded_file($_FILES['imagen']['tmp_name'], 'images/'.$nombreImagen);

	//Establezco consulta
	$sql="INSERT INTO noticias(tituloNoticia, textoNoticia, imagenNoticia, idCategoria)VALUES('$titulo', '$texto', '$nombreImagen', '$categoria')";

	//ejecuto la consulta
	if($consulta=$conexion->query($sql)){
		//header('location:index.php?p=noticias.php');
		header('Refresh: 2; url=index.php?p=noticias.php');
		?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Realizado con exito
		</div>
		<?php
	}else{
		?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}

}else{
	//Muestro el formulario de insercion
?>

<form action="index.php?p=insertar.php" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="titulo">Titulo de la noticia:</label>
		<input type="text" class="form-control" name="titulo" id="titulo">
	</div>

	<div class="form-group">
		<label for="texto">Texto de la noticia:</label>
		<textarea class="form-control" name="texto" id="texto"></textarea>
	</div>

	<div class="form-group">
		<label for="imagen">Imagen de la noticia:</label>
		<input type="file" class="form-control" name="imagen" id="imagen">
	</div>

	<div class="form-group">
		<label for="categoria">Categoria de la noticia:</label>
		<select name="categoria" id="categoria" class="form-control">
			<?php  
			$sqlCat="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
			$consultaCat=$conexion->query($sqlCat);
			while($registroCat=$consultaCat->fetch_array()){
			?>
			<option value="<?php echo $registroCat['idCategoria']; ?>">
				<?php echo $registroCat['nombreCategoria']; ?>
			</option>
			<?php 
			} 
			?>
		</select>
	</div>

	<button type="submit" name="enviar" class="btn btn-default">
		Enviar
	</button>

</form> 

<?php } ?>