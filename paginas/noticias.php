


<h3>
	Estás en el listado de noticias
	-
	<small>
		<a href="index.php?p=insertar.php">INSERTAR NOTICIA</a>
	</small>
</h3>

<?php  
//2.- Establecer la pregunta que quiero hacerle
// preguntamos en lenguaje SQL (Entre comillas dobles)
if(isset($_GET['idCategoria'])){
	$idCategoria=$_GET['idCategoria'];
	$sql="SELECT * FROM noticias INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria WHERE noticias.idCategoria=$idCategoria";
}else{
	$sql="SELECT * FROM noticias INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria";
}

//3.- Ejecuto la pregunta o consulta
$consulta=$conexion->query($sql);

if($consulta->num_rows==0){
	echo 'No se han encontrado noticias';
}


//4.- Procesamos los resultados de la consulta
while($registro=$consulta->fetch_array()){
	?>
	<article>
		<header>
			<h4>
				<a href="index.php?p=detalle.php&idNoticia=<?php echo $registro['idNoticia'];?>">
				<strong>
				<?php echo $registro['tituloNoticia']; ?>
				</strong>
				</a>
				-
				<a href="index.php?p=borrar.php&idNoticia=<?php echo $registro['idNoticia'];?>">
					<span class="glyphicon glyphicon-trash" style="color: red;"></span>
				</a>
				-
				<a href="index.php?p=modificar.php&idNoticia=<?php echo $registro['idNoticia'];?>">
					<span class="glyphicon glyphicon-pencil" style="color: green;"></span>
				</a>	
			</h4>
			<small>
				<?php echo $registro['nombreCategoria'];?>
			</small>
		</header>
		<section>

			<img src="images/<?php echo $registro['imagenNoticia']; ?>" class="img-responsive img-rounded" style="float:left; margin:10px; width: 200px;">

			<?php echo substr($registro['textoNoticia'],0,200); ?> 
			
			<a href="index.php?p=detalle.php&idNoticia=<?php echo $registro['idNoticia'];?>">
			... Leer mas.
			</a>

		</section>
		<footer class="text-right" style="clear: both;">
			<?php echo $registro['fechaNoticia']; ?>
		</footer>
	</article>
	<hr>
	<?php
}


?>