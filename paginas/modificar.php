


<h3>
	Modificar una noticia
	-
	<small>
		<a href="index.php?p=noticias.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if(isset($_POST['enviar'])){
	//modifico la noticia
	//Recojo los datos que quiero modificar
	$titulo=$_POST['titulo'];
	$texto=$_POST['texto'];
	$nombreImagen=$_FILES['imagen']['name']; //Nombre del archivo imagen
	$id=$_POST['idNoticia'];

	$categoria=$_POST['categoria'];

	if(is_uploaded_file($_FILES['imagen']['tmp_name'])){

		//MUEVO la imagen a la carpeta
		move_uploaded_file($_FILES['imagen']['tmp_name'], 'images/'.$nombreImagen);

		//Establezco consulta
		$sql="UPDATE noticias SET tituloNoticia='$titulo', textoNoticia='$texto', imagenNoticia='$nombreImagen', idCategoria='$categoria' WHERE idNoticia=$id";

	}else{
		
		//Establezco consulta
		$sql="UPDATE noticias SET tituloNoticia='$titulo', textoNoticia='$texto', idCategoria='$categoria' WHERE idNoticia=$id";

	}


	//ejecuto la consulta
	if($consulta=$conexion->query($sql)){
		//header('location:index.php?p=noticias.php');
		header('Refresh: 2; url=index.php?p=noticias.php');
		?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Realizado con exito
		</div>
		<?php
	}else{
		?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}

}else{
	//Muestro el formulario de modificacion
	//Necesito el id de la noticia que quiero modificar
	//para rellenar el formulario con los datos de dicha noticia
	$idNoticia=$_GET['idNoticia'];
	$sql="SELECT * FROM noticias WHERE idNoticia=$idNoticia";
	$consulta=$conexion->query($sql);
	$registro=$consulta->fetch_array();
?>

<form action="index.php?p=modificar.php" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="titulo">Titulo de la noticia:</label>
		<input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $registro['tituloNoticia']; ?>">
	</div>

	<div class="form-group">
		<label for="texto">Texto de la noticia:</label>
		<textarea class="form-control" name="texto" id="texto"><?php echo $registro['textoNoticia']; ?></textarea>
	</div>

	<div class="form-group">
		<label for="imagen">Imagen de la noticia (Selecciona otra imagen para modificarla):</label>
		<br>
		<img src="images/<?php echo $registro['imagenNoticia']; ?>" width="100">
		<br>
		<input type="file" class="form-control" name="imagen" id="imagen">
	</div>

	
	<div class="form-group">
		<label for="categoria">Categoria de la noticia:</label>
		<select name="categoria" id="categoria" class="form-control">
			<?php  
			$sqlCat="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
			$consultaCat=$conexion->query($sqlCat);
			while($registroCat=$consultaCat->fetch_array()){
				if($registro['idCategoria']==$registroCat['idCategoria']){
					$seleccionada='selected';
				}else{
					$seleccionada='';
				}
			?>
			<option value="<?php echo $registroCat['idCategoria']; ?>" <?php echo $seleccionada; ?>>
				<?php echo $registroCat['nombreCategoria']; ?>
			</option>
			<?php 
			} 
			?>
		</select>
	</div>	






	<input type="hidden" name="idNoticia" value="<?php echo $idNoticia; ?>">

	<button type="submit" name="enviar" class="btn btn-default">
		Enviar
	</button>

</form> 

<?php } ?>