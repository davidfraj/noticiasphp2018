

<h3>
	Estás en el gestor de categorias
	-
	<small>
		<a href="index.php?p=insertarCat.php">
			INSERTAR CATEGORIA
		</a>
	</small>
</h3>
<table class="table table-hover table-striped">
	<tr>
		<th>Nombre de la categoria</th>
		<th class="text-right">Acciones a realizar</th>
	</tr>
<?php  
	$sql="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
	$consulta=$conexion->query($sql);
	while($registro=$consulta->fetch_array()){
	?>
		<tr>
			<td><?php echo $registro['nombreCategoria']; ?></td>
			<td class="text-right">

			<a href="index.php?p=modificarCat.php&idCategoria=<?php echo $registro['idCategoria'];?>">Modificar</a>
			-
			<a href="index.php?p=borrarCat.php&idCategoria=<?php echo $registro['idCategoria'];?>">Borrar</a>
			
			</td>
		</tr>
	<?php 
} 
?>
</table>