-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-04-2018 a las 12:11:05
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `noticiasphp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCategoria` int(11) NOT NULL,
  `nombreCategoria` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `nombreCategoria`) VALUES
(1, 'actualidad'),
(2, 'television'),
(3, 'videojuegos'),
(4, 'cine'),
(5, 'series de tv'),
(6, 'libros'),
(7, 'Seccion de Deportes'),
(8, 'e-sports'),
(9, 'musica'),
(15, 'formacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `idNoticia` int(11) NOT NULL,
  `tituloNoticia` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `textoNoticia` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaNoticia` datetime NOT NULL,
  `imagenNoticia` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idNoticia`, `tituloNoticia`, `textoNoticia`, `fechaNoticia`, `imagenNoticia`, `idCategoria`) VALUES
(3, 'Cristina Cifuentes pide que le devuelvan el dinero del máster porque es irregular1', 'La presidenta Cristina Cifuentes, en declaraciones hechas este viernes en la Convención Nacional del PP en Sevilla, ha insistido en que las irregularidades detectadas en el máster universitario que supuestamente cur1só son responsabilidad de la universidad, no de los alumnos, y de hecho ha anunciado que pedirá que le reembolsen el importe que abonó por la matrícula, “incluidos esos seis euros y pico que no sé en concepto de qué eran”.\r\n\r\nCifuentes se ha presentado como la “primera sorprendida” y la “primera perjudicada” por este escándalo y cree que “lo mínimo que pueden hacer” es devolverle el dinero “ya que los daños a mi prestigio profesional no los pueden reparar a estas alturas”. “Me siento estafada”, ha dicho al saber que el máster se había falsificado.', '2018-04-09 00:00:00', 'Mi casa.png', 2),
(8, 'Titulo de hoy, que estamos a jueves 19', 'Este es el texto que quiero mantener ', '0000-00-00 00:00:00', 'Mi casa.png', 2),
(9, 'avzxc', 'zxczxc', '0000-00-00 00:00:00', 'modelo2.png', 2),
(10, 'djhdfjdf', 'dfghdfhdfg', '0000-00-00 00:00:00', 'modelo1.png', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNoticia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
